
# Personal task manager  
  
This application will help you to control your day tasks.  Application **Personal task manager** provide abilities to control daily tasks, mark completed tasks.  This is only demo release. In the future application has be able to calculate your day productivity and  will has feature to create tasks on calendar.  
  
## Main menu  
  
Main menu consist from pages of application, by clicking on which you move on page functional.  At this moment application has only one page working, it`s a task list.  Other features will be implemented in future releases.  
  
## Task list  
  
Page of task list consist several control elements.  First element is text box in which you must enter name of new task.  By clicking on "+" button new task will be added to tasks list component. You can`t add empty task.  
  
Under the text field you can see component of the task list.  He consist from you day tasks. Uncompleted tasks mark as red color, completed task mark as green.  For change task status you must just click on that.  
  
## Calendar  
  
This page will contain calendar with ability to add task by days.  This feature is not ready and will be completed in the next updates.  
  
## Productivity calculator  
  
This page will contain the functionality of productivity calculator.  He will help you to calculate your day productivity based your day tasks and calendar tasks.  This feature is not ready and will be completed in the next updates.