import {Button, Page, TextView} from 'tabris';

/**
 * Class CalendarPage
 *
 * This page only for imitate future functional for test project
 */
export class ProductivityCalculator extends Page {
    constructor(properties) {
        super();
        this.set({title: 'Productivity calculator', ...properties}).append(
            <TextView center>This feature in working</TextView>
        );
    }
}

