import {
    Page,
    Button,
    TextView,
    ScrollView,
    TextInput,
    CollectionView,
    Composite,
    AlertDialog,
    Color
} from 'tabris';

const GREEN = new Color(255, 115, 115);
const RED   = new Color(183 , 255 , 183 );

/**
 * Class TasksList
 *
 * This class for render page for working with task todo list
 * It`s main functional of test application
 */
export class TasksList extends Page {
    constructor(properties) {
        super();

        this.createCell = this.createCell.bind(this);
        this.updateCell = this.updateCell.bind(this);
        this.handleTap  = this.handleTap.bind(this);

        this.handleAddClick   = this.handleAddClick.bind(this);
        this.handleTaskDelete = this.handleTaskDelete.bind(this);

        //Tasks storage, i have no time for use Redux for this project
        this.tasks = [];

        this.fetchFromServer();

        this.set({title: 'Tasks list', ...properties}).append(
            <$>
                <TextView
                          left={10}
                          top='10'
                          font='normal normal 16px condensed'
                          >
                    Enter your new task:
                </TextView>
                <TextInput top={40}
                           left={10}
                           right={70}
                           message='Task name'
                           id='task-text'
                           />
                <Button top={40}
                        right={10}
                        width={50}
                        font='normal normal 28px condensed'
                        onSelect={this.handleAddClick}>
                    +
                </Button>
                <ScrollView direction='vertical' top={105} left={10} right={10} bottom={10}>
                    <CollectionView
                        stretch
                        refreshEnabled
                        id='tasks-list'
                        itemCount={this.tasks.length}
                        cellHeight={50}
                        createCell={this.createCell}
                        updateCell={this.updateCell}
                    />
                </ScrollView>
            </$>
        );
    }

    /**
     * This method imitates request to the server
     * but for test application it`s return fake data
     */
    fetchFromServer() {
        this.tasks = [
            {text: 'Start learn Tabris.js', state: false},
            {text: 'Complete test app', state: false},
            {text: 'Send email on complete', state: false}
        ];
    }

    /**
     * This method imitates sending data to the server
     * but for test application it`s return fake data
     */
    sendToServer() {
        //sending this.tasks to the server
    }

    handleAddClick() {
        const inputText = $(TextInput).only('#task-text').text;

        if (inputText.length === 0) return;

        this.tasks.push({
            text: inputText, state: false
        });

        $(CollectionView).only().insert(this.tasks.length, 1);

        $(TextInput).only('#task-text').text = '';

        this.sendToServer();
    }

    createCell() {
        return (
            <Composite  onTap={this.handleTap}
                        bottom={5}>
                <TextView left={0}  right={60} background={GREEN} top={6} bottom={8} />
                <Button right={10} width={40} onSelect={this.handleTaskDelete}>-</Button>
            </Composite>
        );
    }

    updateCell(cell, index) {
        cell.find(TextView).only().text = '  ' + this.tasks[index].text;

        this.tasks[index].state ?
            cell.find(TextView).only().background = RED :
            cell.find(TextView).only().background = GREEN;
    }

    handleTap({target}) {
        const index = target.parent(CollectionView).itemIndex(target);
        this.tasks[index].state = !this.tasks[index].state;

        $(CollectionView).only().refresh(index);

        this.sendToServer();
    }

    handleTaskDelete({target}) {
        const index = target.parent(CollectionView).itemIndex(target);
        AlertDialog.open('Task "' + this.tasks[index].text + '" was deleted');

        $(CollectionView).only().remove(index, 1);
        $(CollectionView).only().refresh(index);

        this.tasks.splice(index, 1);

        this.sendToServer();
    }
}
