import {Button, Page, TextView} from 'tabris';

/**
 * Class TestPage
 *
 * This class only for experiments with Tabris, do not use it in production
 */
export class TestPage extends Page {
    constructor(properties) {
        super();

        this.count = 1;

        this.handleClick = this.handleClick.bind(this);

        this.set({title: 'Test page', ...properties}).append(
            <$>
                <TextView center>No news yet!</TextView>
                <Button id='bt' onSelect={this.handleClick}>{this.count}</Button>
            </$>
        );
    }

    handleClick() {
        this.find('#bt').set({text: `Pressed ${++this.count} times`});
    }
}

