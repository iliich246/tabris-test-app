import {
    Button,
    contentView,
    NavigationView,
    Stack,
    Page
} from 'tabris';

import { TasksList } from './pages/TasksList';
import { Calendar } from './pages/Calendar';
import { ProductivityCalculator } from './pages/ProductivityCalculator';

import { TestPage } from './pages/TestPage';

contentView.append(
    <NavigationView stretch>
        <Page title='Personal task manager'>
            <Stack center>
                <Button centerX onSelect={
                    () => openTasksListPage()}>Open tasks list page</Button>
                <Button centerX onSelect={
                    () => openCalendarPage()}>Open calendar page</Button>
                <Button centerX onSelect={
                    () => openProductivityCalculatorPage()}>Open productivity calculator page</Button>
                <Button centerX onSelect={
                    () => openTestPage()}>Test page (kill in production)</Button>
            </Stack>
        </Page>
    </NavigationView>
);

function openTasksListPage() {
    $(NavigationView).only().append(
        <TasksList />
    );
}

function openCalendarPage() {
    $(NavigationView).only().append(
        <Calendar />
    );
}

function openProductivityCalculatorPage() {
    $(NavigationView).only().append(
        <ProductivityCalculator />
    );
}

function openTestPage() {
    $(NavigationView).only().append(
        <TestPage />
    );
}
